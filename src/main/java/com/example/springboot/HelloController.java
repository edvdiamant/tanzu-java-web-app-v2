package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		//int x = 0;
		//if (x > 0) return "Greetings from Spring Boot + Tanzu! X ist > 0";
		//return "Greetings from Spring Boot + Tanzu! X<=0, YIPPIE DNS is doof";
		return "Greetings from Spring Boot + Tanzu! v2";
	}
	public String bla() { return "xxxx"; }

}